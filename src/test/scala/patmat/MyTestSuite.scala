package patmat

import java.io.InputStream

import org.scalatest.{FunSuite, Matchers}
import patmat.Huffman.{Fork, Leaf}

import scala.io.Source

/**
  * Created by vgorcinschi on 2/8/17.
  */
class MyTestSuite extends FunSuite with Matchers{

  val leaf = Leaf('C', 1)
  val fork = Huffman.makeCodeTree(Leaf('A', 3), Leaf('B', 2))
  val longSource = "Gloriosissimam ciuitatem Dei siue in hoc temporum cursu, " +
    "cum inter impios peregrinatur ex fide uiuens, siue in illa stabilitate " +
    "sedis aeternae, quam nunc expectat per patientiam..."

  import Huffman._

  test("weight returns Leaf's int value"){
    assert(Huffman.weight(leaf) == 1)
  }

  test("weight returns the correct sum for a Fork"){
    assert(Huffman.weight(fork) == 5)
  }

  test("letters from a test file are correctly read"){
    val answer: List[(Char, Int)] = listOfTuplesFromFile
    answer.foreach(tuple => info(s"${tuple._1} was meat ${tuple._2} times"))
     assert(answer.size == 5)
  }

  private def listOfTuplesFromFile = {
    val inputStream: InputStream = getClass.getResourceAsStream("/chars.txt")
    val tokens = Source.fromInputStream(inputStream).mkString.split("")
    //obtain a List of Chars
    var list = {
      for (i <- tokens) yield i.charAt(0)
    }.toList
    //list should contain only five tuples
    val answer = Huffman.times(list)
    answer
  }

  test("makeOrderedLeafList sorts the Leafs in weight ascending order"){
    val candidate = Huffman.makeOrderedLeafList(listOfTuplesFromFile)
    //i.e. letter e is at the front of the list
    assert(candidate.head.weight ==1)
  }

  test("singleton method test: returns true only if the passed in list is of size 1"){
    assert(Huffman.singleton(List(leaf)))
  }

  test("combine function reduces the CodeTree length by 1"){
    val result = Huffman.combine(List(Leaf('C', 3), Leaf('B', 3)))
    assert(result.size == 1)
  }

  test("until method should return a singleton CodeTree"){
    val candidate = until(singleton, combine)(List(fork, leaf))
    assert(candidate.size ==1)
  }

  test("createCodeTree should produce a Fork on a long text"){
    val result = createCodeTree(longSource.toList)
    assert(result.isInstanceOf[Fork])
    info(s"result's weight is ${result.asInstanceOf[Fork].weight}")
  }

  test("decodeSecret should return the String 'Huffman est cool'"){
    //this has been initially tested in patmat.sc worksheet
    decodedSecret.mkString should equal ("huffmanestcool")
  }

  test("encode should equal decodedSecret when called with the same CodeTree"){
      encode(frenchCode)(decodedSecret) should equal(secret)
  }

  test("function codeBits evaluates correctly for char 'h'"){
    //is based on the test output produced in patmat.sc worksheet
    codeBits(convert(frenchCode))('h') should equal(List(0, 0, 1, 1, 1, 0, 1))
  }
}