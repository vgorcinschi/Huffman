import patmat.Huffman._

import scala.collection.mutable.ListBuffer

def decode(tree: CodeTree, bits: List[Bit]): List[Char] = {

  def getOneChar(shrinkableTree: CodeTree, temporaryBits: List[Bit])
  :(Char, List[Bit]) =
    shrinkableTree match {
      case Leaf(x, _) => (x, temporaryBits)
      case Fork(left, right, _, _) => {
        if(temporaryBits.head == 0)
          getOneChar(left, temporaryBits.tail)
        else
          getOneChar(right, temporaryBits.tail)
      }
    }

  def decodeHelper(temporaryBits: List[Bit], acc: ListBuffer[Char]):ListBuffer[Char] =
    if(temporaryBits.isEmpty) acc
    else {
      val step = getOneChar(tree, temporaryBits)
      decodeHelper(step._2, acc+=step._1)
    }

  decodeHelper(bits, new ListBuffer[Char]).toList
}

val decodeTest = decode(frenchCode, secret)

def encode(tree: CodeTree)(text: List[Char]): List[Bit] ={

  def encodeOneCharacter(shrinkableTree: CodeTree, char: Char,
                        acc: ListBuffer[Bit]):List[Bit] = shrinkableTree match {
    case Leaf(c,_) => if(c == char) { println(s"$c for ${acc.mkString}");acc.toList }else List()
    case Fork(left, right, _, _) if(chars(left).contains(char)) => encodeOneCharacter(left, char,acc+=0)
    case Fork(left, right, _, _)  => encodeOneCharacter(right, char, acc+=1)
  }

  def encodeHelper(tempText: List[Char],
                   acc: List[Bit]):List[Bit]=
    tempText match {
      case List() => acc
      case _ => encodeHelper(tempText.tail,
        acc:::encodeOneCharacter(tree, tempText.head, new ListBuffer[Bit]))
    }

  encodeHelper(text, List())
}

val encodeTest = encode(frenchCode)("huffmanestcool".toList)

convert(frenchCode)

codeBits(convert(frenchCode))('h')